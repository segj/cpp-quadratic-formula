#include <iostream>
#include <string>
#include <vector>
#include <cmath>

using namespace std;


double solve_discriminant(double a, double b, double c) {

	double d = b * b - 4 * a * c;

	// this fixes inaccuracy arising from floating point arithmetics in most cases.
	// I know it's a hack. Maybe I'll improve it later
	if (abs(d) < 0.0000001) {

		d = 0.0;

		cout << "\nDifference between discriminant and 0 smaller than 1E-8"
		     << "\nPresumably a floating point error"
		     << "\nCorrected discriminant to 0\n";
	}

	return d;
}


vector<double> solve_real_roots(double a, double b, double d) {

	d = sqrt(d);  // note that after this d is not the discriminant

	double root1 = (-1 * b + d) / (2 * a);
	double root2 = (-1 * b - d) / (2 * a);

	return {root1, root2};
}


vector<double> solve_imaginary_roots(double a, double b, double d) {

	d = sqrt(-1 * d);  // note that after this d is not the discriminant

	double root1_real = (-1 * b) / (2 * a);
	double root1_imaginary = d / (2 * a);

	double root2_real = (-1 * b) / (2 * a);
	double root2_imaginary = (-1 * d) / (2 * a);

	return {root1_real, root1_imaginary, root2_real, root2_imaginary};
}


double get_coeffigient(string coeffigient_name) {

	cout << "\nEnter coeffigient " << coeffigient_name << "\n>";

	double value;
	string input;
	getline(cin, input);

	try {
		value = stod(input);

	} catch(...) {
		cout << "\nInput must be a number";
		value = get_coeffigient(coeffigient_name);
	}

	if ((coeffigient_name == "a") && (value == 0)) {
		cout << "\nCoeffigient 'a' cannot be zero";
		value = get_coeffigient(coeffigient_name);
	}

	return value;

}


/*
 * roots has 2 elements if discriminant d is positive, and 2 identical elements
 * if it is 0. If d is negative, roots has 4 elements, since each root has a real
 * and an imaginary component.
 */
void print(vector<double> roots, double d) {

	if (d == 0) {
		cout << "\nDiscriminant is 0"
		     << "\nMultiple root: " << roots[0] << "\n\n";

	} else if (d > 0) {
		cout << "\nDiscriminant is " << d
		     << "\nRoots:\n" << roots[0] << "\n" << roots[1] << "\n\n";

	} else {
		cout << "\nDiscriminant is " << d
		     << "\nNo real roots. Imaginary roots:\n";

		string imaginary_sign1 = "";
		string imaginary_sign2 = "";
		if (roots[1] > 0) imaginary_sign1 = "+";
		if (roots[3] > 0) imaginary_sign2 = "+";

		cout << roots[0] << " " << imaginary_sign1 << roots[1] << "i\n";
		cout << roots[2] << " " << imaginary_sign2 << roots[3] << "i\n";
	}
}


int main() {

	cout << "\nThis program solves approximate values for the real or "
	     << "imaginary roots of quadratic functions in the form"
	     << "\n    ax^2 + bx + c = 0"
	     << "\nwhere a, b and c are real numbers. Coeffigients b and c "
	     << "can be 0, but a must not. Use a full stop as decimal "
	     << "separator, e.g. 3.14, not 3,14.\n";

	double a = get_coeffigient("a");
	double b = get_coeffigient("b");
	double c = get_coeffigient("c");

	double d = solve_discriminant(a, b, c);
	vector<double> roots;

	if (d < 0) roots = solve_imaginary_roots(a, b, d);
	else roots = solve_real_roots(a, b, d);

	print(roots, d);
}
