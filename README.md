# Quadratic Formula in C++

COMPILE WITH C++11 OR NEWER STANDARD

This program calculates approximate values for the real and imaginary roots
of quadratic functions.

Not warranted to be accurate, although all of my tests except for very specific
edge cases have been accurate. Even so, the program prints a warning when it's
not clear whether there is a floating point error, or if the discriminant is
actually within the range of ]-1x10^-7, 1x10^-7[
